<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MainController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $req)
    {
        $default_from = time() - 60*60*24*100;
        $default_to =  time();

        $query_currency = $req->valuteID ?? null;
        $query_from = $req->datefrom ?? $default_from;
        $query_to = $req->dateto ?? $default_to;

        $query_from = Carbon::parse($query_from)->timestamp;
        $query_to = Carbon::parse($query_to)->timestamp;


        if($query_currency === null || $query_currency === "0"){
            $result = Currency::
            whereBetween('date', [$query_from, $query_to])->
            get() ?: null;
        }else{
            $result = Currency::
            where('valuteID', $query_currency)->
            whereBetween('date', [$query_from, $query_to])->
            get() ?: null;
        }


        $allValues = Currency::select('name', 'valuteID', 'charCode')->distinct()->get();

        return view('pages.api', compact('result', 'allValues'));
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class RestApiController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * @param string $valId
     * @param string $from
     * @param string $to
     * @return JsonResponse
     */
    public function api(string $valId, string $from, string $to) :JsonResponse
    {
        $rows = ['valuteID', 'numCode', 'charCode', 'name', 'value', 'date'];

        try {
            $query_from = Carbon::parse($from)->timestamp;
            $query_to = Carbon::parse($to)->timestamp;
        }
        catch (\Exception $err) {
            return response()->json([
                'result' => $err->getMessage()
            ], 400);
        }

        $result = Currency::
        select($rows)->
        where('valuteID', $valId)->
        whereBetween('date', [$query_from, $query_to])->
        get()->
        toArray() ?: null;


        return response()->json([
            'result' => $result
        ], 200);
    }
}

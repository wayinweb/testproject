<?php
/**
 *
 * api.blade.php
 * Created By Alex M.
 * 10.03.2022 12:21
 * @cbproject
 */
?>
@extends('layouts.app')

@section('content')
    <!-- Javascript -->
    <script>
        $(function() {
            $( "#datepick1" ).datepicker({
                dateFormat:"dd-mm-yy",
                defaultDate: -10,
            });
            $( "#datepick2" ).datepicker({
                dateFormat:"dd-mm-yy"
            });

        });
    </script>
    <div class="container">
        <div class="row">
            <div class="col-8">

                <form action="" method="get">
                    <div class="form-group mb-4">
                        <select name="valuteID" class="form-control">
                            <option value="0">Все валюты</option>
                            @foreach($allValues as $q)
                                <option value="{{$q->valuteID}}">{{$q->name . " / " . $q->charCode}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <input type="text" id="datepick1" class="form-control" name="datefrom" autocomplete="off" placeholder="С какого числа">
                        </div>
                        <div class="col">
                            <input type="text" id="datepick2" class="form-control" autocomplete="off" name="dateto" placeholder="По какое число">
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Получить</button>
                    </div>
                </form>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Валюта</th>
                        <th>Дата</th>
                        <th>Курс</th>
                    </tr>
                    </thead>
                    <tbody>



                    @foreach($result as $q)
                        <tr>
                            <td>{{ $q->name }} ({{ $q->charCode }})</td>
                            <td>{{ \Carbon\Carbon::parse($q->date)->format('d-m-Y') }}</td>
                            <td>{{ $q->value }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <p></p>
    <p></p>
@endsection

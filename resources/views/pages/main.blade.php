@extends('layouts.app')

@section('content')

    <header class="py-5">
        <div class="container px-lg-5">
            <div class="p-4 p-lg-5 bg-light rounded-3 text-center">
                <div class="m-4 m-lg-5">
                    <h1 class="display-5 fw-bold">Добро пожаловать!</h1>
                    <p class="fs-4">Для работы с приложением требуется автороизация</p>
                    <a class="btn btn-primary btn-lg" href="{{ route('apipage') }}">Начать</a>
                </div>
            </div>
        </div>
    </header>

@endsection

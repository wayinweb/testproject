<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\ProjFuncts;


class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Получим массив кодов всех валют в array $valIds
         */
        $res = ProjFuncts::xmlToArray(\Http::get('https://www.cbr.ru/scripts/XML_daily.asp?date_req=01/12/2021'));

        $valIds = [];
        foreach ($res['Valute'] as $valute) {
            $valIds[] = [
                'vid'       => $valute['@attributes']['ID'],
                'NumCode'   => $valute['NumCode'],
                'CharCode'  => $valute['CharCode'],
                'Name'      => $valute['Name'],
            ];
        }

        $result = [];
        foreach ($valIds as $vId) {
            $cbRes = ProjFuncts::xmlToArray(\Http::get('https://www.cbr.ru/scripts/XML_dynamic.asp', [
                'date_req1' => date('d/m/Y', time() - (60*60*24*30)),
                'date_req2' => date('d/m/Y', time()),
                'VAL_NM_RQ' => $vId['vid'],
            ]));


            foreach ($cbRes['Record'] as $cb){

                $result[] = [
                    'valuteID' => $vId['vid'],
                    'numCode' => (int)$vId['NumCode'],
                    'charCode' => $vId['CharCode'],
                    'name' => $vId['Name'],
                    'value' => (float)str_replace(',','.', $cb['Value']),
                    'date' => Carbon::parse($cb['@attributes']['Date'])->timestamp,
                ];

            }

        }

        \DB::table('currencies')->insert($result);
    }
}
